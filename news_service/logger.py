from django.utils import timezone

def __get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def log(request=None, response=None, *messages):
    ip = __get_client_ip(request) if (request is not None) else 'N/A'
    status_code = response.status_code if (response is not None) else 0
    if status_code >= 200 and status_code < 300:
        status_code = '\033[32m' + str(status_code) + '\033[0m'
    elif status_code >= 400 and status_code < 500:
        status_code = '\033[31m' + str(status_code) + '\033[0m'
    elif status_code >= 500 and status_code < 600:
        status_code = '\033[35m' + str(status_code) + '\033[0m'
        
    request_info = '\033[33m' + str(request.method) + ' ' + str(request.path) + '\033[0m' if (request is not None) else 'N/A'
    message = ' - '.join(str(m) for m in messages)
    print(f'{timezone.now().strftime("%Y/%m/%d - %H:%M:%S")} | {status_code} | {request_info} | {ip} | {message}')