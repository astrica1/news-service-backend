from rest_framework import serializers
from .models import Post, Comment, Like

class PostsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at')
        
class CommentsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'
        read_only_fields = ('create_date', 'update_date')
        
class LikesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Like
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at')