from django.urls import path, include
from rest_framework import routers
from .views import CategoryViewSet

router = routers.DefaultRouter()
router.register(r'categories', CategoryViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('list/', CategoryViewSet.as_view({'get': 'list'}), name='category-list'),
    path('create/', CategoryViewSet.as_view({'post': 'create'}), name='category-create'),
    path('<int:pk>/', CategoryViewSet.as_view({'get': 'retrieve'}), name='category-detail'),
    path('<int:pk>/update/', CategoryViewSet.as_view({'put': 'update'}), name='category-update'),
    path('<int:pk>/delete/', CategoryViewSet.as_view({'delete': 'destroy'}), name='category-delete')
]